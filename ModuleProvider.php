<?php

namespace Lmn\Thread;

use Lmn\Core\Provider\LmnServiceProvider;
use Lmn\Core\Lib\Module\ModuleServiceProvider;
use Illuminate\Support\Facades\Route;
use Lmn\Core\Lib\Database\Seeder\SeederService;

use Lmn\Core\Lib\Relation\RelationService;
use Lmn\Core\Lib\Filter\FilterService;
use Lmn\Core\Lib\Order\OrderService;
use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Account\Lib\Auth\CurrentUser;

use Lmn\Account\Middleware\SigninRequiredMiddleware;
use Lmn\Subject\Repository\SubjectRepository;

use Lmn\Thread\Lib\Thread\ThreadService;
use Lmn\Thread\Lib\Entry\EntryService;

use Lmn\Thread\Database\Seed\ThreadSeeder;
use Lmn\Thread\Database\Seed\TagThreadSeeder;
use Lmn\Thread\Database\Seed\EntrySeeder;

use Lmn\Thread\Database\Validation\CreateThreadFormValidation;
use Lmn\Thread\Database\Validation\ThreadListOldValidation;
use Lmn\Thread\Database\Validation\CreateEntryFormValidation;
use Lmn\Thread\Database\Validation\ThreadvoteValidation;
use Lmn\Thread\Database\Validation\EntryvoteValidation;
use Lmn\Thread\Database\Validation\ThreadPidValidation;
use Lmn\Thread\Repository\EntryRepository;
use Lmn\Thread\Repository\ThreadRepository;
use Lmn\Thread\Repository\Criteria\Entry\EntryFirstCriteria;
use Lmn\Thread\Repository\Criteria\Entry\EntriesDefaultCriteria;
use Lmn\Thread\Repository\Criteria\Entry\EntryWithVoteCriteria;
use Lmn\Thread\Repository\Criteria\Entry\EntryDefaultCriteria;
use Lmn\Thread\Repository\Criteria\Thread\WithTagsCriteria;
use Lmn\Thread\Repository\Criteria\Thread\WithEntriesCriteria;
use Lmn\Thread\Repository\Criteria\Thread\ThreadDetailCriteria;
use Lmn\Thread\Repository\Criteria\Thread\ThreadCountCriteria;
use Lmn\Thread\Repository\Criteria\Thread\ThreadUnreadCountCriteria;
use Lmn\Thread\Repository\Criteria\Thread\ThreadDefaultCriteria;
use Lmn\Thread\Repository\Criteria\Thread\ThreadBySubjectCriteria;
use Lmn\Thread\Repository\Criteria\Thread\ThreadBySubjectsCriteria;
use Lmn\Thread\Repository\Criteria\Thread\ThreadBySubjectPidCriteria;
use Lmn\Thread\Repository\Criteria\Thread\ThreadBySubjectPidsCriteria;
use Lmn\Thread\Repository\Criteria\Thread\ThreadByPublicIdCriteria;
use Lmn\Thread\Repository\Criteria\Thread\WithFirstEntryCriteria;
use Lmn\Thread\Repository\Criteria\Thread\ThreadWithSubjectCriteria;
use Lmn\Thread\Repository\Criteria\Entryvote\EntryvoteUniqueCriteria;
use Lmn\Thread\Repository\Criteria\Entryvote\EntryvoteCountvotesCriteria;
use Lmn\Thraed\Repository\Criteria\TagThread\TagTrendingCriteria;
use Lmn\Thraed\Repository\Criteria\TagThread\TagByThreadCriteria;
use Lmn\Thraed\Repository\Criteria\TagThread\ThreadsByTagCriteria;
use Lmn\Thread\Repository\EntryvoteRepository;
use Lmn\Thread\Repository\TagThreadRepository;
use Lmn\Thread\Repository\Listener\ThreadUnreadCountListener;

class ModuleProvider implements ModuleServiceProvider{

    public function register(LmnServiceProvider $provider) {
        $app = $provider->getApp();
        // Thread service
        $app->singleton(ThreadService::class, ThreadService::class);

        // Entry service
        $app->singleton(EntryService::class, EntryService::class);

        $app->singleton(EntryRepository::class, EntryRepository::class);
        $app->singleton(ThreadRepository::class, ThreadRepository::class);
        $app->singleton(EntryvoteRepository::class, EntryvoteRepository::class);
        $app->singleton(TagThreadRepository::class, TagThreadRepository::class);

        $this->registerCommands($provider);
    }

    public function boot(LmnServiceProvider $provider) {
        $app = $provider->getApp();

        $seederService = \App::make(SeederService::class);
        $seederService->addSeeder(ThreadSeeder::class);
        $seederService->addSeeder(TagThreadSeeder::class);
        $seederService->addSeeder(EntrySeeder::class);

        $validationService = \App::make(ValidationService::class);
        $validationService->add('form.thread.create', CreateThreadFormValidation::class);
        $validationService->add('thread.list.old', ThreadListOldValidation::class);
        $validationService->add('form.entry.create', CreateEntryFormValidation::class);
        $validationService->add('thread.vote', ThreadvoteValidation::class);
        $validationService->add('entry.vote', EntryvoteValidation::class);
        $validationService->add('thread.by.publicId', ThreadPidValidation::class);

        /** @var CriteriaService $criteriaService */
        $criteriaService = $app->make(CriteriaService::class);
        $criteriaService->add('thread.default', ThreadDefaultCriteria::class);
        $criteriaService->add('thread.by.subject', ThreadBySubjectCriteria::class);
        $criteriaService->add('thread.by.subjects', ThreadBySubjectsCriteria::class);
        $criteriaService->add('thread.by.subjectpid', ThreadBySubjectPidCriteria::class);
        $criteriaService->add('thread.by.subjectpids', ThreadBySubjectPidsCriteria::class);
        $criteriaService->add('thread.by.pid', ThreadByPublicIdCriteria::class);
        $criteriaService->add('thread.with.first', WithFirstEntryCriteria::class);
        $criteriaService->add('thread.with.tags', WithTagsCriteria::class);
        $criteriaService->add('thread.with.entries', WithEntriesCriteria::class);
        $criteriaService->add('thread.detail', ThreadDetailCriteria::class);
        $criteriaService->add('thread.count', ThreadCountCriteria::class);
        $criteriaService->add('thread.unread', ThreadUnreadCountCriteria::class);
        $criteriaService->add('entries.default', EntriesDefaultCriteria::class);
        $criteriaService->add('entry.first', EntryFirstCriteria::class);
        $criteriaService->add('entry.with.vote', EntryWithVoteCriteria::class);
        $criteriaService->add('entry.default', EntryDefaultCriteria::class);
        $criteriaService->add('entryvote.unique', EntryvoteUniqueCriteria::class);
        $criteriaService->add('entryvote.countvotes', EntryvoteCountvotesCriteria::class);
        $criteriaService->add('tag.trending', TagTrendingCriteria::class);
        $criteriaService->add('tag.by.thread', TagByThreadCriteria::class);
        $criteriaService->add('thread.with.subject', ThreadWithSubjectCriteria::class);
        $criteriaService->add('thread.by.tag', ThreadsByTagCriteria::class);

        $subjectRepository = $app->make(SubjectRepository::class);
        $subjectRepository->on('*', new ThreadUnreadCountListener($app->make(ThreadRepository::class), $app->make(CurrentUser::class)));
    }

    public function event(LmnServiceProvider $provider) {

    }

    public function route(LmnServiceProvider $provider) {
        Route::group(['namespace' => 'Lmn\\Thread\\Controller'], function() {
            Route::post('api/thread/list','ThreadController@getList')->middleware(SigninRequiredMiddleware::class);
            Route::post('api/thread/list_all','ThreadController@getListAll')->middleware(SigninRequiredMiddleware::class);
            Route::post('api/thread/detail','ThreadController@getDetail')->middleware(SigninRequiredMiddleware::class);
            Route::post('api/thread/create','ThreadController@create')->middleware(SigninRequiredMiddleware::class);
            Route::post('api/thread/update/{publicId}','ThreadController@update')->middleware(SigninRequiredMiddleware::class);
            Route::post('api/thread/vote','ThreadController@vote')->middleware(SigninRequiredMiddleware::class);
            Route::post('api/thread/subject','ThreadController@getSubject')->middleware(SigninRequiredMiddleware::class);

            Route::post('api/thread/tag/recommend','TagThreadController@recommend')->middleware(SigninRequiredMiddleware::class);
            Route::post('api/thread/tag/list','TagThreadController@getThreads')->middleware(SigninRequiredMiddleware::class);

            Route::post('api/entry/create','EntryController@create')->middleware(SigninRequiredMiddleware::class);
            //Route::post('api/entry/update/{id}','EntryController@update')->middleware(SigninRequiredMiddleware::class);
            Route::post('api/entry/vote','EntryController@vote')->middleware(SigninRequiredMiddleware::class);
        });
    }

    public function registerCommands($provider){

    }
}
