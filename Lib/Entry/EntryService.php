<?php

namespace Lmn\Thread\Lib\Entry;

use Lmn\Core\Lib\Database\Save\SaveService;

use Lmn\Account\Repository\UserRepository;

use Lmn\Thread\Database\Model\Thread;
use Lmn\Thread\Database\Model\Entry;
use Lmn\Thread\Repository\EntryRepository;
use Lmn\Thread\Repository\EntryvoteRepository;

class EntryService {

    private $entryRepo;
    private $entryvoteRepo;
    private $userRepo;

    public function __construct(EntryRepository $entryRepo, EntryvoteRepository $entryvoteRepo, UserRepository $userRepo) {
        $this->entryRepo = $entryRepo;
        $this->entryvoteRepo = $entryvoteRepo;
        $this->userRepo = $userRepo;
    }

    public function create($data) {
        $saveService = \App::make(SaveService::class);

        $entry = new Entry();
        $entry->user_id = 1;
        $entry->fill($data);

        $saveService->begin($entry)
            ->transaction();

        $entry->thread;

        return $entry;
    }

    public function update($data) {

    }

    public function loadUser($entryDetail) {
        $user = $entryDetail->user;
        // TODO: use more secure repository (that only loads avatar from settings)
        $this->userRepo->onGet($user);
    }

    public function loadUsers($entries) {
        $users = [];
        foreach ($entries as $e) {
            $users[] = $e->user;
        }
        // TODO: use more secure repository (that only loads avatar from settings)
        $this->userRepo->onList($users);
    }

    public function refreshVote($entryId) {
        $entryvote = $this->entryvoteRepo->clear()
            ->criteria('entryvote.countvotes', ['entryId' => $entryId])
            ->first();

        $this->entryRepo->clear()
            ->criteria('core.id', ['id' => $entryId])
            ->update(['votevalue' => $entryvote->votes]);
    }

    public function normalizeVote($vote) {
        if ($vote > 1) {
            return 1;
        }
        else if ($vote < -1) {
            return -1;
        }
        else {
            return $vote;
        }
    }
}
