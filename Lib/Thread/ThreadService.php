<?php

namespace Lmn\Thread\Lib\Thread;

use Lmn\Core\Lib\Database\GeneratorService;
use Lmn\Core\Lib\Database\Save\SaveService;

use Lmn\Account\Lib\Auth\CurrentUser;
use Lmn\Account\Repository\UserRepository;
use Lmn\Subject\Repository\SubjectUserRepository;
use Lmn\Subject\Lib\Subject\SubjectUserService;
use Lmn\Notification\Lib\Notification\NotificationService;
use Lmn\Notification\Lib\Notification\NotificationMessage;

use Lmn\Thread\Database\Model\Thread;
use Lmn\Thread\Database\Model\Entry;
use Lmn\Thread\Repository\ThreadRepository;
use Lmn\Thread\Repository\TagThreadRepository;

class ThreadService {

    private $subjectUserRepo;
    private $threadRepo;
    private $notification;
    private $currentUser;
    private $userRepo;
    private $tagThreadRepo;
    private $subjectUserService;

    public function __construct(SubjectUserRepository $subjectUserRepo, ThreadRepository $threadRepo, NotificationService $notification, CurrentUser $currentUser, UserRepository $userRepo, TagThreadRepository $tagThreadRepo, SubjectUserService $subjectUserService) {
        $this->notification = $notification;
        $this->subjectUserRepo = $subjectUserRepo;
        $this->currentUser = $currentUser;
        $this->threadRepo = $threadRepo;
        $this->userRepo = $userRepo;
        $this->tagThreadRepo = $tagThreadRepo;
        $this->subjectUserService = $subjectUserService;
    }

    private function getListDefault($subjectPid) {
        return $this->getListDefaultAll()
            ->criteria('thread.by.subjectpid', ['subjectPid' => $subjectPid])
            ->criteria('thread.with.entries', ['userId' => $this->currentUser->getId()]);
    }

    private function getListDefaultAll() {
        return $this->threadRepo->clear()
            ->criteria('thread.default')
            ->criteria('thread.with.first', ['userId' => $this->currentUser->getId()])
            ->criteria('thread.with.tags')
            ->criteria('core.limit', ['limit' => 12]);
    }

    public function getList($subjectPid) {
        $list = $this->getListDefault($subjectPid)->all();

        $this->loadListUsers($list);
        return $list;
    }

    public function getListNew($subjectPid, $since) {
        return $this->getListDefault($subjectPid)
            ->criteria('core.since', ['since' => $since])
            ->all();
    }

    public function getListOld($subjectPid, $until) {
        $list = $this->getListDefault($subjectPid)
            ->criteria('core.until', ['until' => $until, 'table' => 'thread'])
            ->all();

        $this->loadListUsers($list);
        return $list;
    }

    public function loadUsers($threadDetail) {
        $users[] = $threadDetail->first_entry->user;
        foreach ($threadDetail->entries as $e) {
            $users[] = $e->user;
        }
        // TODO: use more secure repository (that only loads avatar from settings)
        $this->userRepo->onList($users);
    }

    public function loadListUsers($list) {
        $users = [];
        foreach ($list as $l) {
            $users[] = $l->first_entry->user;
            foreach ($l->entries as $e) {
                $users[] = $e->user;
            }
        }
        // TODO: use more secure repository (that only loads avatar from settings)
        $this->userRepo->onList($users);
    }

    public function getDetail($publicId) 
    {
        $detail = $this->getListDefaultAll()
            ->criteria('thread.by.pid', ['publicId' => $publicId])
            ->criteria('thread.with.entries', ['userId' => $this->currentUser->getId()])
            ->first();

        if (!$detail) {
            return $detail;
        }        
        $this->loadUsers($detail);
        return $detail;
    }

    public function getListAll($config = []) {
        $subjects = $this->subjectUserService->forUser($this->currentUser->getId());
        $subjectPids = [];
        foreach ($subjects as $s) {
            $subjectPids[] = $s->subjectprototype->public_id;
        }

        $listQuery = $this->getListDefaultAll()
            ->criteria('thread.by.subjectpids', ['subjectPids' => $subjectPids]);

        if (isset($config['until'])) {
            $listQuery->criteria('core.until', ['until' => $config['until'], 'table' => 'thread']);
        }

        $list = $listQuery->all();

        $users = [];
        foreach ($list as $l) {
            $users[] = $l->first_entry->user;
            foreach ($l->entries as $e) {
                $users[] = $e->user;
            }
        }
        // TODO: use more secure repository (that only loads avatar from settings)
        $this->userRepo->onList($users);

        return $list;
    }

    private function getSubjectUsers($thread) {
        $subjectUsers = $this->subjectUserRepo->clear()
            ->criteria('user.by.subject', ['subjectId' => $thread->subject_id])
            ->all();

        $users = [];
        foreach ($subjectUsers as $user) {
            if ($user->id != $this->currentUser->getId()) {
                $users[] = $user->id;
            }
        }

        return $users;
    }

    private function pushThread($users, $thread, $route) {
        return $this->notification->build()
            ->route($route)
            ->to($users)
            ->body([
                'thread_id' => $thread->id,
                'subject_id' => $thread->subject_id
            ])
            ->title("Sombody voted");
            //->description("User [".$data['user_id']."] voted for your post with value [".$votevalue."]")
    }

    public function pushThreadCreate($thread) {
        $users = $this->getSubjectUsers($thread);

        return $this->pushThread($users, $thread, 'thread.create');
    }

    public function pushThreadDetail($thread) {
        $users = $this->getSubjectUsers($thread);

        return $this->pushThread($users, $thread, 'thread.detail');
    }

    private function genPublicId() {
        $generator = \App::make(GeneratorService::class);
        return $generator->uniqueString('thread', 'public_id');
    }

    public function getActive($subjectprototypeId) {

    }

    public function getUnreadCount($userId) {
        $unread = $this->subjectUserRepo->clear()
            ->criteria('subjectuser.by.user', ['userId' => $userId])
            ->criteria('thread.unread')
            ->all();

        $result = [];
        foreach ($unread as $un) {
            $result[$un->subject_id] = $un->unread;
        }

        return $result;
    }

    public function addTags($threadId, $tags) 
    {
        $tagsExists = $this->tagThreadRepo->clear()
            ->criteria('tag.by.thread', ['threadId' => $threadId])
            ->all();

        $tagsExistsIds = [];
        foreach ($tagsExists as $t) {
            $tagsExistsIds[$t->tag_id] = $t;
        }
        
        foreach ($tags as $t) {
            if (isset($tagsExistsIds[$t])) {
                continue;
            }
            $this->tagThreadRepo->clear()
                ->create([
                'thread_id' => $threadId,
                'tag_id' => $t
            ]);
        }
    }

    public function create($data) {
        $saveService = \App::make(SaveService::class);

        $thread = new Thread();
        $thread->fill($data);
        $thread->user_id = 1;
        $thread->public_id = $this->genPublicId();

        $entry = new Entry();
        if (isset($data['entry'])) {
            $entry->fill($data['entry']);
        }
        $entry->user_id = 1;
        $entry->firstentry = 1;

        $saveService->begin()
            ->has($entry, $thread)
            ->transaction();

        $entry->thread;

        return $entry;
    }

    public function update($data) {

    }
}
