<?php

namespace Lmn\Thread\Controller;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Lmn\Tag\Lib\Tag\TagService;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Notification\Lib\Notification\NotificationService;
use Lmn\Notification\Lib\Notification\NotificationMessage;

use Lmn\Subject\Repository\SubjectRepository;
use Lmn\Subject\Repository\SubjectUserRepository;
use Lmn\Subject\Lib\Subject\SubjectUserService;

use Lmn\Thread\Lib\Thread\ThreadService;
use Lmn\Thread\Lib\Entry\EntryService;
use Lmn\Account\Lib\Auth\AuthService;
use Lmn\Account\Lib\Auth\CurrentUser;

use Lmn\Thread\Database\Model\Thread;
use Lmn\Thread\Database\Model\Entry;
use Lmn\Thread\Repository\EntryRepository;
use Lmn\Thread\Repository\ThreadRepository;
use Lmn\Thread\Repository\EntryvoteRepository;

class ThreadController extends Controller {

    public function getList(Request $request, ResponseService $responseService, ValidationService $validationService, CurrentUser $currentUser, SubjectUserService $subjectUserService, ThreadRepository $repo, ThreadService $threadService) {
        $data = $request->json()->all();

        if (!$validationService->systemValidate($data, 'subject.public_id')) {
            return $responseService->use('validation.system');
        }
        $time = time();

        $responseOptions = [];
        if (isset($data['until'])) {
            $list = $threadService->getListOld($data['subject_pid'], $data['until']);
        } else {
            $list = $threadService->getList($data['subject_pid']);
            $responseOptions['timestamp'] = $time;
            $subjectUserService->seen($data['subject_pid'], $currentUser->getId(), Carbon::now());
        }

        $list = $list->all();
        $response = $responseService->createMessage($list);
        $responseOptions['totalItems'] = count($list);
        $response->setOption($responseOptions);

        return $responseService->send($response);
    }

    public function getListAll(Request $request, ResponseService $responseService, ThreadService $threadService) {
        $data = $request->json()->all();

        $list = $threadService->getListAll($data);

        $list = $list->all();
        $response = $responseService->createMessage($list);
        $response->setOption([
            'totalItems' => sizeof($list)
        ]);

        return $responseService->send($response);
    }

    public function getDetail(Request $request, ResponseService $responseService, ValidationService $validationService, CurrentUser $currentUser, SubjectUserService $subjectUserService, ThreadRepository $repo, ThreadService $threadService) {
        $data = $request->json()->all();

        if (!$validationService->systemValidate($data, 'thread.by.publicId')) {
            return $responseService->use('validation.system');
        }

        $detail = $threadService->getDetail($data['public_id']);

        $response = $responseService->createMessage($detail);

        return $responseService->send($response);
    }

    public function getSubject(Request $request, ResponseService $responseService, ValidationService $validationService, ThreadRepository $threadRepo) {
        $data = $request->json()->all();

        if (!$validationService->systemValidate($data, 'thread.by.publicId')) {
            return $responseService->use('validation.system');
        }

        $thread = $threadRepo->clear()
            ->criteria('thread.by.pid', ['publicId' => $data['public_id']])
            ->criteria('thread.with.subject')
            ->get();

        return $responseService->response($thread->subject);
    }

    public function create(Request $request, ResponseService $responseService, ValidationService $validationService, CurrentUser $currentUser, SubjectRepository $subjectRepo, TagService $tagService, ThreadRepository $threadRepo, ThreadService $threadService) {
        $data = $request->json()->all();

        if (!$validationService->systemValidate($data, 'subject.public_id')) {
            return $responseService->use('validation.system');
        }

        $subject = $subjectRepo->clear()
            ->criteria('subject.default')
            ->criteria('subject.by.publicId', ['publicId' => $data['subject_pid']])
            ->get();

        $data['tags'] = [];
        $data['user_id'] = $currentUser->getId();
        $data['subject_id'] = $subject->id;
        $data['first_entry']['user_id'] = $currentUser->getId();

        if (!$validationService->validate($data, 'form.thread.create')) {
            return $responseService->use('validation.data');
        }

        $tags = $tagService->extractTags($data['first_entry']['text']);
        $data['tags'] = $tagService->getIds($tags);
        if (isset($data['first_entry']['files']) && !empty($data['first_entry']['files'])) {
            $data['tags'][] = $tagService->getSystemTag('file');
        }

        $thread = $threadRepo->clear()
            ->create($data);

        $thread = $threadRepo->clear()
            ->criteria('core.id', ['id' => $thread->id])
            ->criteria('thread.with.first', ['userId' => $currentUser->getId()])
            ->criteria('thread.with.entries', ['userId' => $currentUser->getId()])
            ->criteria('thread.with.tags')
            ->get();
        $threadService->loadUsers($thread);

        $threadService->pushThreadCreate($thread)->send();

        return $responseService->response($thread);
    }

    public function update($publicId) {

    }

    public function vote(Request $request, AuthService $authService, ResponseService $responseService, ValidationService $validationService, EntryService $entryService, EntryvoteRepository $entryvoteRepo, ThreadRepository $threadRepo, EntryRepository $entryRepo, ThreadService $threadService) {
        $data = $request->json()->all();

        $data['user_id'] = $authService->getCurrentUser()->id;
        if (!$validationService->validate($data, 'entry.vote')) {
            return $responseService->use('validation.data');
        }

        $entryDetail = $entryRepo->clear()
            ->criteria('core.id', ['id' => $data['entry_id']])
            ->get();

        $entryvote = $entryvoteRepo->clear()
            ->criteria('entryvote.unique', ['entryId' => $data['entry_id'], 'userId' => $data['user_id']])
            ->first();

        $votevalue = $entryService->normalizeVote($data['value']);
        // create new vote
        if ($entryvote == null) {
            $entryvoteRepo->clear()
                ->create($data);
        }
        // update existing vote
        else {
            if ($entryvote->value == $votevalue) {
                $votevalue = 0;
            }
            $entryvoteRepo->clear()
                ->criteria('entryvote.unique', ['entryId' => $data['entry_id'], 'userId' => $data['user_id']])
                ->update(['value' => $votevalue]);
        }

        $entryService->refreshVote($data['entry_id']);

        $detail = $threadRepo->clear()
            ->criteria('core.id', ['id' => $entryDetail->thread_id])
            ->criteria('thread.with.first', ['userId' => $data['user_id']])
            ->criteria('thread.with.entries', ['userId' => $data['user_id']])
            ->criteria('thread.with.tags')
            ->get();

        $threadService->loadUsers($detail);
        $threadService->pushThreadDetail($detail)->send();

        return $responseService->response($detail);
    }
}
