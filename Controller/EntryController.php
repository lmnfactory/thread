<?php

namespace Lmn\Thread\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Filter\FilterService;
use Lmn\Core\Lib\Order\OrderService;
use Lmn\Core\Lib\Relation\RelationService;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Lib\Model\ValidationService;

use Lmn\Account\Lib\Auth\CurrentUser;
use Lmn\Notification\Repository\NotificationRepository;
use Lmn\Notification\Lib\NotificationDb\NotificationService;
use Lmn\tag\Lib\Tag\TagService;

use Lmn\Thread\Lib\Entry\EntryService;
use Lmn\Thread\Lib\Thread\ThreadService;
use Lmn\Thread\Database\Model\Thread;
use Lmn\Thread\Database\Model\Entry;
use Lmn\Thread\Repository\ThreadRepository;
use Lmn\Thread\Repository\EntryRepository;
use Lmn\Thread\Repository\EntryvoteRepository;

class EntryController extends Controller {

    public function create(Request $request, ResponseService $responseService, ValidationService $validationService, EntryRepository $entryRepo, CurrentUser $currentUser, ThreadRepository $threadRepo, ThreadService $threadService, NotificationRepository $notificationRepo, NotificationService $notificationService, TagService $tagService, EntryService $entryService) {
        $data = $request->json()->all();

        $data['user_id'] = $currentUser->getId();
        if (!$validationService->validate($data, 'form.entry.create')) {
            return $responseService->use('validation.data');
        }

        $entry = $entryRepo->clear()
            ->create($data);

        $entries = $entryRepo->clear()
            ->criteria('entries.default', ['threadId' => $data['thread_id']])
            ->all();
        $entryService->loadUsers($entries);

        $threadDetail = $threadRepo->clear()
            ->criteria('core.id', ['id' => $data['thread_id']])
            ->update([
                'entrycount' => \DB::raw('entrycount+1')
            ]);
        $threadDetail = $threadRepo->clear()
            ->criteria('core.id', ['id' => $data['thread_id']])
            ->first();

        if (isset($data['files']) && !empty($data['files'])) {
            $tags = [$tagService->getSystemTag('file')];
            $threadService->addTags($threadDetail->id, $tags);
        }

        $threadService->pushThreadDetail($threadDetail)->send();

        // Notify creator of the thread
        if ($currentUser->getId() != $threadDetail->user_id) {
            $notification = $notificationRepo->clear()
            ->create([
                'source' => 'entry.create',
                'source_id' => $entry->id,
                'user_id' => $threadDetail->user_id,
                'title' => $currentUser->getFullname() . ' komentoval tvoj prispevok',
                'text' => substr($entry->text, 0, 255),
                'options' => ['threadPid' => $threadDetail->public_id]
            ]);

        $notificationService->pushNotification($notification)->send();
        }

        $message = $responseService->createMessage($entries);
        $message->addOption('threadId', $entry->thread_id);
        return $responseService->send($message);
    }

    public function update($id) {

    }

    public function vote(Request $request, CurrentUser $currentUser, ResponseService $responseService, ValidationService $validationService, EntryService $entryService, EntryvoteRepository $entryvoteRepo, EntryRepository $entryRepo, ThreadRepository $threadRepo, ThreadService $threadService) {
        $data = $request->json()->all();

        $data['user_id'] = $currentUser->getId();
        if (!$validationService->validate($data, 'entry.vote')) {
            return $responseService->use('validation.data');
        }

        $entryvote = $entryvoteRepo->clear()
            ->criteria('entryvote.unique', ['entryId' => $data['entry_id'], 'userId' => $data['user_id']])
            ->first();

        $votevalue = $entryService->normalizeVote($data['value']);
        // create new vote
        if ($entryvote == null) {
            $entryvoteRepo->clear()
                ->create($data);
        }
        // update existing vote
        else {
            if ($entryvote->value == $votevalue) {
                $votevalue = 0;
            }
            $entryvoteRepo->clear()
                ->criteria('entryvote.unique', ['entryId' => $data['entry_id'], 'userId' => $data['user_id']])
                ->update(['value' => $votevalue]);
        }

        $entryService->refreshVote($data['entry_id']);

        $entryDetail = $entryRepo->clear()
            ->criteria('core.id', ['id' => $data['entry_id']])
            ->criteria('entry.default', [])
            ->criteria('entry.with.vote', ['userId' => $data['user_id']])
            ->get();
        $entryService->loadUser($entryDetail);

        $threadDetail = $threadRepo->clear()
            ->criteria('core.id', ['id' => $entryDetail->thread_id])
            ->criteria('thread.with.first', ['userId' => $data['user_id']])
            ->criteria('thread.with.entries', ['userId' => $data['user_id']])
            ->criteria('thread.with.tags')
            ->get();

        $threadService->pushThreadDetail($threadDetail)->send();

        return $responseService->response($entryDetail);
    }
}
