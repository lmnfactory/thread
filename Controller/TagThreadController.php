<?php

namespace Lmn\Thread\Controller;

use App\Http\Controllers\Controller;
use Lmn\Tag\Lib\Tag\TagService;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Lib\Model\ValidationService;

use Lmn\Thread\Lib\Thread\ThreadService;
use Lmn\Thread\Lib\Entry\EntryService;
use Lmn\Account\Lib\Auth\AuthService;
use Lmn\Account\Lib\Auth\CurrentUser;
use Lmn\Subject\Lib\Subject\SubjectUserService;

use Lmn\Thread\Database\Model\Thread;
use Lmn\Thread\Database\Model\Entry;
use Lmn\Thread\Repository\EntryRepository;
use Lmn\Thread\Repository\ThreadRepository;
use Lmn\Thread\Repository\EntryvoteRepository;
use Lmn\Thread\Repository\TagThreadRepository;

class TagThreadController extends Controller {

    public function recommend(Request $request, ResponseService $responseService, TagThreadRepository $tagThreadRepo) {
        // week before
        $since = time() - 7 * 24 * 60 * 60;
        $threadTags = $tagThreadRepo->clear()
            ->criteria('tag.trending', ['since' => $since])
            ->criteria('core.top', ['limit' => 10])
            ->all();
        
        if (empty($threadTags)) {
            // month before
            $since = time() - 30 * 24 * 60 * 60;
            $threadTags = $tagThreadRepo->clear()
                ->criteria('tag.trending', ['since' => $since])
                ->criteria('core.top', ['limit' => 10])
                ->all();
        }

        if (empty($threadTags)) {
            // year before
            $since = time() - 365 * 24 * 60 * 60;
            $threadTags = $tagThreadRepo->clear()
                ->criteria('tag.trending', ['since' => $since])
                ->criteria('core.top', ['limit' => 10])
                ->all();
        }

        $tags = [];
        foreach ($threadTags as $t) {
            $tags[] = $t->tag;
        }

        return $responseService->response($tags);
    }

    public function getThreads(Request $request, ResponseService $responseService, TagThreadRepository $tagThreadRepo, SubjectUserService $subjectUserService, CurrentUser $currentUser) {
        $data = $request->json()->all();

        $subjects = $subjectUserService->forUser($currentUser->getId());
        $subjectIds = [];
        foreach ($subjects as $s) {
            $subjectIds[] = $s->subject_id;
        }
        
        $tagThreadsQuery = $tagThreadRepo->clear()
            ->criteria('thread.default')
            ->criteria('thread.with.first', ['userId' => $currentUser->getId(), 'tablePrefix' => 'thread'])
            ->criteria('thread.by.tag', ['slug' => $data['slug']])
            ->criteria('thread.by.subjects', ['subjectIds' => $subjectIds])
            ->criteria('core.limit', ['limit' => 12]);
        
        if (isset($data['until'])) {
            $tagThreadsQuery->criteria('core.until', ['until' => $data['until'], 'table' => 'thread']);
        }

        $tagThreads = $tagThreadsQuery->all();

        $list = [];
        foreach ($tagThreads as $tt) {
            $list[] = $tt->thread;
        }

        $response = $responseService->createMessage($list);
        $response->setOption([
            'totalItems' => count($list)
        ]);

        return $responseService->send($response);
    }
}
