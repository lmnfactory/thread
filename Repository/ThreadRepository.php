<?php

namespace Lmn\Thread\Repository;
use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Database\Save\SaveService;
use Lmn\Core\Lib\Database\GeneratorService;
use Lmn\Thread\Database\Model\Entry;
use Lmn\Thread\Database\Model\Thread;
use Lmn\Thread\Database\Model\Tag_thread;
use Lmn\Thread\Database\Model\Entry_file;

class ThreadRepository extends AbstractEloquentRepository {

    private $saveService;
    private $generatorService;

    public function __construct(CriteriaService $criteriaService, SaveService $saveService, GeneratorService $generatorService) {
        parent::__construct($criteriaService);
        $this->saveService = $saveService;
        $this->generatorService = $generatorService;
    }

    public function getModel() {
        return Thread::class;
    }

    public function create($data) {
        $model = $this->getModel();
        $thread = new $model();
        $thread->fill($data);
        $thread->public_id = $this->generatorService->uniqueString('thread', 'public_id');

        $entry = new Entry();
        if (isset($data['first_entry'])) {
            $entry->fill($data['first_entry']);
        }
        $entry->firstentry = 1;

        $threadTags = [];
        if (isset($data['tags'])) {
            foreach ($data['tags'] as $tag) {
                $threadTag = new Tag_thread();
                $threadTag->fill(['tag_id' => $tag]);
                $threadTags[] = $threadTag;
            }
        }

        $files = [];
        if (isset($data['first_entry']) && isset($data['first_entry']['files'])) {
            foreach ($data['first_entry']['files'] as $file) {
                $entryFile = new Entry_file();
                $entryFile->fill(['file_id' => $file]);
                $files[] = $entryFile;
                // TODO: confirm files in db
            }
        }
        

        $builder = $this->saveService->begin()
            ->has($entry, $thread);
        foreach ($threadTags as $tag) {
            $builder->has($tag, $thread);
        }
        foreach ($files as $file) {
            $builder->has($file, $entry);
        }
        $builder->transaction();

        $thread->first_entry;
        $thread->tags;

        return $thread;
    }
}
