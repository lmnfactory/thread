<?php

namespace Lmn\Thread\Repository;
use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Database\Save\SaveService;
use Lmn\Core\Lib\Database\GeneratorService;
use Lmn\Thread\Database\Model\Entry;
use Lmn\Thread\Database\Model\Thread;
use Lmn\Thread\Database\Model\Tag_thread;

class TagThreadRepository extends AbstractEloquentRepository {

    private $saveService;

    public function __construct(CriteriaService $criteriaService, SaveService $saveService) {
        parent::__construct($criteriaService);
        $this->saveService = $saveService;
    }

    public function getModel() {
        return Tag_thread::class;
    }

    public function create($data) {
        $model = $this->getModel();
        $threadTag = new $model();
        $threadTag->fill($data);

        $this->saveService->begin($threadTag)
            ->transaction();

        return $threadTag;
    }
}
