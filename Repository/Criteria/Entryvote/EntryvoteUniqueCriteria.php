<?php

namespace Lmn\Thread\Repository\Criteria\Entryvote;

use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Lmn\Core\Lib\Repository\Criteria\EloquentCriteriaService;
use Illuminate\Database\Eloquent\Builder;

class EntryvoteUniqueCriteria implements Criteria {

    private $userId;
    private $entryId;

    public function __construct() {

    }

    public function set($data) {
        $this->userId = $data['userId'];
        $this->entryId = $data['entryId'];
    }

    public function apply(Builder $builder) {
        $builder->where('entryvote.user_id', '=', $this->userId)
            ->where('entryvote.entry_id', '=', $this->entryId);
    }
}
