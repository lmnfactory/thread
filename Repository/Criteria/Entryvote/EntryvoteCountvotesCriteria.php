<?php

namespace Lmn\Thread\Repository\Criteria\Entryvote;

use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Lmn\Core\Lib\Repository\Criteria\EloquentCriteriaService;
use Illuminate\Database\Eloquent\Builder;

class EntryvoteCountvotesCriteria implements Criteria {

    private $entryId;

    public function __construct() {

    }

    public function set($data) {
        $this->entryId = $data['entryId'];
    }

    public function apply(Builder $builder) {
        $builder->select(\DB::raw('SUM(entryvote.value) as votes'))
            ->where('entryvote.entry_id', '=', $this->entryId);
    }
}
