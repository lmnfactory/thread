<?php

namespace Lmn\Thread\Repository\Criteria\Thread;
use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Lmn\Core\Lib\Repository\Criteria\EloquentCriteriaService;
use Illuminate\Database\Eloquent\Builder;

class WithTagsCriteria implements Criteria {

    public function __construct() {

    }

    public function set($data) {

    }

    public function apply(Builder $builder) {
        $builder->with(['tags']);
    }
}
