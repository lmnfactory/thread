<?php

namespace Lmn\Thread\Repository\Criteria\Thread;

use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Lmn\Core\Lib\Repository\Criteria\EloquentCriteriaService;
use Illuminate\Database\Eloquent\Builder;

class WithEntriesCriteria implements Criteria {

    private $userId;

    public function __construct() {

    }

    public function set($data) {
        $this->userId = $data['userId'];
    }

    public function apply(Builder $builder) {
        $userId = $this->userId;
        $builder->with(['entries' => function($query) use ($userId) {
                $query->with(['vote' => function($query) use ($userId) {
                        $query->where('user_id', '=', $userId);
                    }])
                    ->where('entry.firstentry', '=', 0)
                    ->orderBy('entry.created_at', 'asc');
            }, 'entries.user', 'entries.files']);
    }
}
