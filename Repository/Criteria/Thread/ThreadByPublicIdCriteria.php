<?php

namespace Lmn\Thread\Repository\Criteria\Thread;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Repository\EloquentRepository;
use Illuminate\Database\Eloquent\Builder;

class ThreadByPublicIdCriteria implements Criteria {

    private $publicId;
    private $criteriaService;

    public function __construct(CriteriaService $criteriaService) {
        $this->criteriaService = $criteriaService;
    }

    public function set($data) {
        $this->publicId = $data['publicId'];
    }

    public function apply(Builder $builder) {
        $builder->where('thread.public_id', '=', $this->publicId);
    }
}
