<?php

namespace Lmn\Thread\Repository\Criteria\Thread;

use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Lmn\Core\Lib\Repository\Criteria\EloquentCriteriaService;
use Illuminate\Database\Eloquent\Builder;

class ThreadBySubjectPidsCriteria implements Criteria {

    private $subjectPids;
    public function __construct() {

    }

    public function set($data) {
        $this->subjectPids = $data['subjectPids'];
    }

    public function apply(Builder $builder) {
        $builder->select(['thread.*'])
            ->join('subject', 'subject.id', '=', 'thread.subject_id')
            ->join('subjectprototype', 'subjectprototype.id', '=', 'subject.subjectprototype_id')
            ->whereIn('subjectprototype.public_id', $this->subjectPids)
            ->where('subject.active', '=', true)
            ->orderBy('thread.created_at', 'DESC');
    }
}
