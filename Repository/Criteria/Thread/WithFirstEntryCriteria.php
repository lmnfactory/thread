<?php

namespace Lmn\Thread\Repository\Criteria\Thread;
use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Lmn\Core\Lib\Repository\Criteria\EloquentCriteriaService;
use Illuminate\Database\Eloquent\Builder;

class WithFirstEntryCriteria implements Criteria {

    private $userId;
    private $tablePrefix;

    public function __construct() {
        $this->tablePrefix = null;
    }

    private function getWith() {
        $with = "first_entry";
        if ($this->tablePrefix != null) {
            $with = $this->tablePrefix . "." . $with;
        }
        return $with;
    }

    public function set($data) {
        $this->userId = $data['userId'];
        if (isset($data['tablePrefix'])) {
            $this->tablePrefix = $data['tablePrefix'];
        }
    }

    public function apply(Builder $builder) {
        $userId = $this->userId;
        $builder->select(['thread.*'])
            ->with([$this->getWith() => function($query) use ($userId) {
                $query->with(['vote' => function($query) use ($userId) {
                        $query->where('user_id', '=', $userId);
                    }, 'user', 'files'])
                    ->where('entry.firstentry', '=', true);
            }]);
    }
}
