<?php

namespace Lmn\Thread\Repository\Criteria\Thread;

use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Lmn\Core\Lib\Repository\Criteria\EloquentCriteriaService;
use Illuminate\Database\Eloquent\Builder;

class ThreadBySubjectCriteria implements Criteria {

    private $subjectId;
    public function __construct() {

    }

    public function set($data) {
        $this->subjectId = $data['subjectId'];
    }

    public function apply(Builder $builder) {
        $builder->where('thread.subject_id', '=', $this->subjectId);
    }
}
