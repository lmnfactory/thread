<?php

namespace Lmn\Thread\Repository\Criteria\Thread;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class ThreadUnreadCountCriteria implements Criteria {

    private $userId;

    public function __construct() {
        $this->userId = null;
    }

    public function set($args) {
        $this->userId = $args['currentUser'];
    }

    public function apply(Builder $builder) {
        $userId = $this->userId;
        $builder->selectRaw('COUNT(*) as unread, thread.subject_id')
            ->join('subject_user', function($join) use ($userId) {
                $join->on('subject_user.subject_id', '=', 'thread.subject_id')
                    ->on('subject_user.user_id', '=', \DB::raw($userId));
            })
            ->where('thread.created_at', '>', \DB::raw('subject_user.lastvisit'))
            ->where('thread.user_id', '!=', $this->userId)
            ->groupBy('thread.subject_id');
    }
}
