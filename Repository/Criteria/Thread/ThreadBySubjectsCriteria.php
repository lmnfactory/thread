<?php

namespace Lmn\Thread\Repository\Criteria\Thread;

use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Lmn\Core\Lib\Repository\Criteria\EloquentCriteriaService;
use Illuminate\Database\Eloquent\Builder;

class ThreadBySubjectsCriteria implements Criteria {

    private $subjectIds;
    public function __construct() {

    }

    public function set($data) {
        $this->subjectIds = $data['subjectIds'];
    }

    public function apply(Builder $builder) {
        $builder->whereIn('thread.subject_id', $this->subjectIds);
    }
}
