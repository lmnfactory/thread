<?php

namespace Lmn\Thread\Repository\Criteria\Thread;

use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Lmn\Core\Lib\Repository\Criteria\EloquentCriteriaService;
use Illuminate\Database\Eloquent\Builder;

class ThreadDetailCriteria implements Criteria {

    private $publicId;

    public function __construct() {

    }

    public function set($data) {
        $this->publicId = $data['publicId'];
    }

    public function apply(Builder $builder) {
        $builder->with(['entries'])
            ->where('thread.id', '=', $this->publicId);
    }
}
