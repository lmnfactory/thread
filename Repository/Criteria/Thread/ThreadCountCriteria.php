<?php

namespace Lmn\Thread\Repository\Criteria\Thread;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class ThreadCountCriteria implements Criteria {

    private $subjectId;

    public function __construct() {

    }

    public function set($args) {

    }

    public function apply(Builder $builder) {
        $builder->selectRaw('COUNT(*) as count, thread.subejct_id')
            ->groupBy('thread.subject_id');
    }
}
