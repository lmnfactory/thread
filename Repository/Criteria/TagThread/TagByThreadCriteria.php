<?php

namespace Lmn\Thraed\Repository\Criteria\TagThread;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class TagByThreadCriteria implements Criteria {

    private $threadId;

    public function __construct() {

    }

    public function set($args) {
        $this->threadId = $args['threadId'];
    }

    public function apply(Builder $query) {
        $query
            ->select(['tag_id'])
            ->where('thread_id', '=', $this->threadId);
    }
}
