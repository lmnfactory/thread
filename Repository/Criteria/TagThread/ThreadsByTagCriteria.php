<?php

namespace Lmn\Thraed\Repository\Criteria\TagThread;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class ThreadsByTagCriteria implements Criteria {

    private $tagSlug;

    public function __construct() {

    }

    public function set($args) {
        $this->tagSlug = $args['slug'];
    }

    public function apply(Builder $query) {
        $query->select(['thread_id'])
            ->with(['thread', 'thread.tags'])
            ->join('tag', 'tag_thread.tag_id', '=', 'tag.id')
            ->join('thread', 'tag_thread.thread_id', '=', 'thread.id')
            ->where('tag.slug', '=', $this->tagSlug);
    }
}
