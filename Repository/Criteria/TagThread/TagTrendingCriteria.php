<?php

namespace Lmn\Thraed\Repository\Criteria\TagThread;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class TagTrendingCriteria implements Criteria {

    private $since;

    public function __construct() {

    }

    public function set($args) {
        $this->since = $args['since'];
    }

    public function apply(Builder $query) {
        $query->select(['tag_id'])
            ->join('tag', 'tag_thread.tag_id', '=', 'tag.id')
            ->with(['tag'])
            ->where('tag_thread.created_at', '>', $this->since)
            ->where('tag.system', '=', false)
            ->groupBy('tag_thread.tag_id')
            ->orderBy(\DB::raw('COUNT(*)'), 'desc');
    }
}
