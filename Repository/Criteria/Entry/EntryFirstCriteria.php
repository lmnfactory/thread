<?php

namespace Lmn\Thread\Repository\Criteria\Entry;
use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Lmn\Core\Lib\Repository\Criteria\EloquentCriteriaService;
use Illuminate\Database\Eloquent\Builder;

class EntryFirstCriteria implements Criteria {

    public function __construct() {

    }

    public function set($data) {

    }

    public function apply(Builder $builder) {
        $builder->select(['entry.*'])
            ->with(['thread'])
            ->join('thread', 'thread.id', '=', 'entry.thread_id')
            ->where('entry.firstentry', '=', true);
    }
}
