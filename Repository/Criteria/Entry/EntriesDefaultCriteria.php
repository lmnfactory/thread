<?php

namespace Lmn\Thread\Repository\Criteria\Entry;
use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Lmn\Core\Lib\Repository\Criteria\EloquentCriteriaService;
use Illuminate\Database\Eloquent\Builder;

class EntriesDefaultCriteria implements Criteria {

    private $threadId;

    public function __construct() {

    }

    public function set($data) {
        $this->threadId = $data['threadId'];
    }

    public function apply(Builder $builder) {
        $builder->with(['user', 'files'])
            ->where('entry.thread_id', '=', $this->threadId)
            ->where('entry.firstentry', '=', false)
            ->orderBy('entry.created_at', 'ASC');
    }
}
