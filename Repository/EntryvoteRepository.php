<?php

namespace Lmn\Thread\Repository;
use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Database\Save\SaveService;
use Lmn\Thread\Database\Model\Entryvote;

class EntryvoteRepository extends AbstractEloquentRepository {

    private $saveService;

    public function __construct(CriteriaService $criteriaService, SaveService $saveService) {
        parent::__construct($criteriaService);
        $this->saveService = $saveService;
    }

    public function getModel() {
        return Entryvote::class;
    }

    public function create($data) {
        $model = $this->getModel();
        $entryvote = new $model();
        $entryvote->fill($data);

        $this->saveService->begin($entryvote)
            ->transaction();

        return $entryvote;
    }
}
