<?php

namespace Lmn\Thread\Repository;
use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Database\Save\SaveService;
use Lmn\Thread\Database\Model\Entry;
use Lmn\Thread\Database\Model\Entry_file;

class EntryRepository extends AbstractEloquentRepository {

    private $saveService;

    public function __construct(CriteriaService $criteriaService, SaveService $saveService) {
        parent::__construct($criteriaService);
        $this->saveService = $saveService;
    }

    public function getModel() {
        return Entry::class;
    }

    public function create($data) {
        $model = $this->getModel();
        $entry = new $model();
        $entry->fill($data);

        $files = [];
        if (isset($data['files'])) {
            foreach ($data['files'] as $file) {
                $entryFile = new Entry_file();
                $entryFile->fill(['file_id' => $file]);
                $files[] = $entryFile;
                // TODO: confirm files in db
            }
        }

        $builder = $this->saveService->begin($entry);
        foreach ($files as $file) {
            $builder->has($file, $entry);
        }
        $builder->transaction();

        return $entry;
    }
}
