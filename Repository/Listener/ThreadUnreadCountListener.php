<?php

namespace Lmn\Thread\Repository\Listener;

use Lmn\Core\Lib\Repository\EloquentRepository;
use Lmn\Core\Lib\Repository\AbstractListenerEloquentRepository;
use Lmn\Account\Lib\Auth\CurrentUser;

class ThreadUnreadCountListener extends AbstractListenerEloquentRepository {

    private $repo;
    private $currentUser;

    public function __construct(EloquentRepository $repo, CurrentUser $currentUser) {
        $this->repo = $repo;
        $this->currentUser = $currentUser;
    }

    public function onGet($model) {
        $this->repo->clear()
            ->criteria('thread.by.subject', ['subjectId' => $model->id])
            ->criteria('thread.unread', ['currentUser' => $this->currentUser->getId()]);
        $model->unread = $this->repo->get();
    }

    public function onList($models) {
        $ids = [];
        foreach ($models as $m) {
            $ids[] = $m->id;
        }


        $list = $this->repo->clear()
            ->criteria('thread.by.subjects', ['subjectIds' => $ids])
            ->criteria('thread.unread', ['currentUser' => $this->currentUser->getId()])
            ->all();

        $listSorted = [];
        foreach ($list as $l) {
            $listSorted[$l->subject_id] = $l;
        }
        foreach ($models as $model) {
            if (!isset($listSorted[$model->id])) {
                $model->unread = 0;
                continue;
            }
            $model->unread = $listSorted[$model->id]->unread;
        }
    }
}
