<?php

namespace Lmn\Thread\Build\Test;

use Lmn\Account\Lib\Test\AuthTestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

class ThreadControllerTest extends AuthTestCase {

    use DatabaseTransactions;

    // TODO: not signed in actions

    public function testList() {
        $this->authJson('POST', '/api/thread/list', [
                'subject_pid' => '3TciReiHTJi9pklppzfUM8dsIHonx81F'
            ])
            ->assertResponseOk()
            ->seeJsonStructure([
                'data',
                'option' => [ 'totalItems', 'timestamp' ]
            ]);
    }

    public function testListInvalid() {
        $this->authJson('POST', '/api/thread/list', [
                'subject_pid' => 'VunqFtVeSU9ZhtJsrM600oom4fNKk'
            ])
            ->assertResponseStatus(400);

        $this->authJson('POST', '/api/thread/list', [])
            ->assertResponseStatus(400);
    }

    public function testListOld() {
        $this->authJson('POST', '/api/thread/list', [
                'subject_pid' => '3TciReiHTJi9pklppzfUM8dsIHonx81F',
                'until' => date("Y-n-d H:i:s")
            ])
            ->assertResponseOk()
            ->seeJsonStructure([
                'data',
                'option' => [ 'totalItems' ]
            ]);
    }

    public function testListOldInvalid() {
        $this->authJson('POST', '/api/thread/list', [
                'subject_pid' => 'VunqFtVeSU9ZhtJsrM600oom4fNKk',
                'until' => date("Y-n-d H:i:s")
            ])
            ->assertResponseStatus(400);

        $this->authJson('POST', '/api/thread/list', [
                'until' => date("Y-n-d H:i:s")
            ])
            ->assertResponseStatus(400);
    }

    public function testCreate() {
        $this->authJson('POST', '/api/thread/create', [
                'subject_pid' => '3TciReiHTJi9pklppzfUM8dsIHonx81F',
                'title' => 'Test',
                'first_entry' => [
                    'text' => 'This is just a test #test'
                ]
            ])
            ->assertResponseOk()
            ->seeJsonStructure([
                'data' => [ 'id', 'title', 'subject_id',
                    'first_entry' => [ 'text', 'firstentry' ],
                    'tags' => [ 0 ]
                ]
            ]);

        $this->authJson('POST', '/api/thread/create', [
                'subject_pid' => '3TciReiHTJi9pklppzfUM8dsIHonx81F',
                'title' => 'Test',
                'first_entry' => [
                    'text' => 'This is just a test'
                ]
            ])
            ->assertResponseOk()
            ->seeJsonStructure([
                'data' => [ 'id', 'title', 'subject_id',
                    'first_entry' => [ 'text', 'firstentry' ],
                    'tags'
                ]
            ]);
    }

    public function testCreateInvalid() {
        // wrong subject public id
        $this->authJson('POST', '/api/thread/create', [
                'subject_pid' => '3TciReiHTJi9pklppzfUM8dsIHonx',
                'title' => 'Test',
                'first_entry' => [
                    'text' => 'This is just a test #test'
                ]
            ])
            ->assertResponseStatus(400);

        // no subject public id
        $this->authJson('POST', '/api/thread/create', [
                'title' => 'Test',
                'first_entry' => [
                    'text' => 'This is just a test #test'
                ]
            ])
            ->assertResponseStatus(400);

        // no title
        $this->authJson('POST', '/api/thread/create', [
                'subject_pid' => '3TciReiHTJi9pklppzfUM8dsIHonx81F',
                'first_entry' => [
                    'text' => 'This is just a test #test'
                ]
            ])
            ->assertResponseStatus(422);

        // empty title
        $this->authJson('POST', '/api/thread/create', [
                'subject_pid' => '3TciReiHTJi9pklppzfUM8dsIHonx81F',
                'title' => '',
                'first_entry' => [
                    'text' => 'This is just a test #test'
                ]
            ])
            ->assertResponseStatus(422);

        // empty text
        $this->authJson('POST', '/api/thread/create', [
                'subject_pid' => '3TciReiHTJi9pklppzfUM8dsIHonx81F',
                'title' => 'Test',
                'first_entry' => [
                    'text' => ''
                ]
            ])
            ->assertResponseStatus(422);

        // no text
        $this->authJson('POST', '/api/thread/create', [
                'subject_pid' => '3TciReiHTJi9pklppzfUM8dsIHonx81F',
                'title' => 'Test',
                'first_entry' => []
            ])
            ->assertResponseStatus(422);

        //no first_entry
        $this->authJson('POST', '/api/thread/create', [
                'subject_pid' => '3TciReiHTJi9pklppzfUM8dsIHonx81F',
                'title' => 'Test'
            ])
            ->assertResponseStatus(422);
    }

    public function testThreadVote() {
        $this->authJson('POST', '/api/thread/vote', [
                'entry_id' => 1,
                'value' => 1
            ])
            ->assertResponseOk()
            ->seeJsonStructure([
                'data' => [ 'id', 'title', 'subject_id',
                    'first_entry' => [ 'text', 'firstentry' ],
                    'entries',
                    'entrycount',
                    'tags' => [ ]
                ]
            ]);
    }

    public function testThreadVoteInvalid() {
        // not existing entry
        $this->authJson('POST', '/api/thread/vote', [
                'entry_id' => 10007,
                'value' => 1
            ])
            ->assertResponseStatus(422);

        // no entry
        $this->authJson('POST', '/api/thread/vote', [
                'value' => 1
            ])
            ->assertResponseStatus(422);

        // no value
        $this->authJson('POST', '/api/thread/vote', [
                'entry_id' => 1
            ])
            ->assertResponseStatus(422);
    }

    //TODO: thread detail
    //TODO: thread update
}
