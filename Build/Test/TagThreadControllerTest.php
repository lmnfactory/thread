<?php

namespace Lmn\Thread\Build\Test;

use Lmn\Account\Lib\Test\AuthTestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

class TagThreadControllerTest extends AuthTestCase {

    use DatabaseTransactions;

    public function testTagThreadRecommend() {
        $this->authJson('POST', '/api/thread/tag/recommend', [])
            ->assertResponseOk()
            ->seeJsonStructure([
                'data' => [ 0, 1 ]
            ]);
    }

    public function testTagThreadRecommendUnauthorise() {
        $this->json('POST', '/api/thread/tag/recommend', [])
            ->assertResponseStatus(401);
    }
}
