<?php

namespace Lmn\Thread\Build\Test;

use Lmn\Account\Lib\Test\AuthTestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

class EntryControllerTest extends AuthTestCase {

    use DatabaseTransactions;

    public function testEntryCreate() {
        $this->authJson('POST', '/api/entry/create', [
                'thread_id' => 1,
                'text' => 'test'
            ])
            ->assertResponseOk()
            ->seeJsonStructure([
                'data' => [ 0 ],
                'option' => [
                    'threadId'
                ]
            ]);
    }

    public function testEntryCreateUnauthorise() {
        $this->json('POST', '/api/entry/create', [
                'thread_id' => 1,
                'text' => 'test'
            ])
            ->assertResponseStatus(401);
    }

    public function testEntryCreateInvalid() {
        // empty text
        $this->authJson('POST', '/api/entry/create', [
                'thread_id' => 1,
                'text' => ''
            ])
            ->assertResponseStatus(422);

        // no text
        $this->authJson('POST', '/api/entry/create', [
                'thread_id' => 1
            ])
            ->assertResponseStatus(422);

        // invalid thread id
        $this->authJson('POST', '/api/entry/create', [
                'thread_id' => 100,
                'text' => 'test'
            ])
            ->assertResponseStatus(422);

        // no thread
        $this->authJson('POST', '/api/entry/create', [
                'text' => 'test'
            ])
            ->assertResponseStatus(422);
    }

    public function testEntryVote() {
        $this->authJson('POST', '/api/entry/vote', [
                'entry_id' => 1,
                'value' => 1
            ])
            ->assertResponseOk()
            ->seeJsonStructure([
                'data' => [ 'thread_id', 'id', 'text', 'user' ]
            ]);

        $this->authJson('POST', '/api/entry/vote', [
                'entry_id' => 1,
                'value' => -5
            ])
            ->assertResponseOk()
            ->seeJsonStructure([
                'data' => [ 'thread_id', 'id', 'text' ]
            ]);
    }

    public function testEntryVoteUnautorize() {
        $this->json('POST', '/api/entry/vote', [
                'entry_id' => 1,
                'value' => 1
            ])
            ->assertResponseStatus(401);
    }

    public function testEntryVoteInvalid() {
        // wrong entry id
        $this->authJson('POST', '/api/entry/vote', [
                'entry_id' => 100,
                'value' => 1
            ])
            ->assertResponseStatus(422);

        //no entry id
        $this->authJson('POST', '/api/entry/vote', [
                'value' => -5
            ])
            ->assertResponseStatus(422);

        // no value
        $this->authJson('POST', '/api/entry/vote', [
                'entry_id' => 1
            ])
            ->assertResponseStatus(422);

        // null value
        $this->authJson('POST', '/api/entry/vote', [
                'entry_id' => 100,
                'value' => null
            ])
            ->assertResponseStatus(422);
    }

    //TODO: entry update
}
