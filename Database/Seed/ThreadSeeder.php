<?php

namespace Lmn\Thread\Database\Seed;

use App;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ThreadSeeder extends Seeder {

    public function run() {
        $env = App::environment();
        if ($env == "production") {
            return;
        }

        \DB::table('thread')->insert([
            [
                'id' => 1,
                'user_id' => 1,
                'subject_id' => 1,
                'public_id' => 'aaa',
                'title' => "Otazka?",
                'entrycount' => 0,
                'created_at' => Carbon::now()->subDays(1)->format('Y-m-d H:i:s')
            ],
            [
                'id' => 2,
                'user_id' => 1,
                'subject_id' => 2,
                'public_id' => 'bbb',
                'title' => "co sa ucit na skuska",
                'entrycount' => 1,
                'created_at' => Carbon::now()->subDays(2)->subHours(18)->format('Y-m-d H:i:s')
            ],
            [
                'id' => 3,
                'user_id' => 1,
                'subject_id' => 3,
                'public_id' => 'ccc',
                'title' => null,
                'entrycount' => 2,
                'created_at' => Carbon::now()->subDays(1)->subHours(3)->format('Y-m-d H:i:s')
            ]
        ]);
    }
}
