<?php

namespace Lmn\Thread\Database\Seed;

use App;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class EntrySeeder extends Seeder {

    public function run() {
        $env = App::environment();
        if ($env == "production") {
            return;
        }

        \DB::table('entry')->insert([
            [
                'id' => 1,
                'user_id' => 1,
                'thread_id' => 1,
                'text' => "kedy je otvorene studijne?",
                'firstentry' => 1,
                'created_at' => Carbon::now()->subDays(1)->format('Y-m-d H:i:s')
            ],
            [
                'id' => 2,
                'user_id' => 1,
                'thread_id' => 2,
                'text' => "mate nejake materialy, lebo ja som celkom mimo",
                'firstentry' => 1,
                'created_at' => Carbon::now()->subDays(2)->subHours(18)->format('Y-m-d H:i:s')
            ],
            [
                'id' => 3,
                'user_id' => 1,
                'thread_id' => 3,
                'text' => "ako sa dostanem do historie na tejto stranke",
                'firstentry' => 1,
                'created_at' => Carbon::now()->subDays(1)->subHours(3)->format('Y-m-d H:i:s')
            ],
            [
                'id' => 4,
                'user_id' => 1,
                'thread_id' => 3,
                'text' => "hore pod nazvom predmetu mas tab ze historia, tak to klikni.",
                'firstentry' => 0,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id' => 5,
                'user_id' => 1,
                'thread_id' => 2,
                'text' => "na tu mas link",
                'firstentry' => 0,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id' => 6,
                'user_id' => 1,
                'thread_id' => 3,
                'text' => "@marek ja tam nic take nevidim :(",
                'firstentry' => 0,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }
}
