<?php

namespace Lmn\Thread\Database\Seed;

use App;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TagThreadSeeder extends Seeder {

    public function run() {
        $env = App::environment();
        if ($env == "production") {
            return;
        }
        
        \DB::table('tag_thread')->insert([
            [
                'id' => 1,
                'thread_id' => 1,
                'tag_id' => 3,
                'created_at' => Carbon::now()->subDays(1)->format('Y-m-d H:i:s')
            ],
            [
                'id' => 2,
                'thread_id' => 1,
                'tag_id' => 2,
                'created_at' => Carbon::now()->subDays(1)->format('Y-m-d H:i:s')
            ],
            [
                'id' => 3,
                'thread_id' => 2,
                'tag_id' => 2,
                'created_at' => Carbon::now()->subDays(2)->format('Y-m-d H:i:s')
            ]
        ]);
    }
}
