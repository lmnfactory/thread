<?php

namespace Lmn\Thread\Database\Validation;

use Lmn\Core\Lib\Model\LaravelValidation;

class CreateThreadFormValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'subject_id' => 'required|exists:subject,id',
            'user_id' => 'required|exists:user,id',
            'title' => 'required|string|max:255',
            'first_entry.text' => 'required|string'
        ];
    }
}
