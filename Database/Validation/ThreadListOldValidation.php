<?php

namespace Lmn\Thread\Database\Validation;

use Lmn\Core\Lib\Model\LaravelValidation;

class ThreadListOldValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'subject_pid' => 'required|exists:subjectprototype,public_id',
            'until' => 'required|date'
        ];
    }
}
