<?php

namespace Lmn\Thread\Database\Validation;

use Lmn\Core\Lib\Model\LaravelValidation;

class CreateEntryFormValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'thread_id' => 'required|exists:thread,id',
            'user_id' => 'required|exists:user,id',
            'text' => 'required'
        ];
    }
}
