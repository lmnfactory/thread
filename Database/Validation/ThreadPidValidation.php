<?php

namespace Lmn\Thread\Database\Validation;

use Lmn\Core\Lib\Model\LaravelValidation;

class ThreadPidValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'public_id' => 'required|exists:thread,public_id'
        ];
    }
}
