<?php

namespace Lmn\Thread\Database\Validation;

use Lmn\Core\Lib\Model\LaravelValidation;

class EntryvoteValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'entry_id' => 'required|exists:entry,id',
            //'user_id' => 'required|exists:user,id',
            'value' => 'required'
        ];
    }
}
