<?php

namespace Lmn\Thread\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Entrythank extends Model {

    protected $table = 'entrythank';

    protected $fillable = ['user_id', 'entry_id'];
}
