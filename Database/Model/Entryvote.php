<?php

namespace Lmn\Thread\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Entryvote extends Model {

    protected $table = 'entryvote';

    protected $fillable = ['user_id', 'entry_id', 'value'];
}
