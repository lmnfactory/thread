<?php

namespace Lmn\Thread\Database\Model;

use Illuminate\Database\Eloquent\Model;
use Lmn\Tag\Database\Model\Tag;
use Lmn\Thread\Database\Model\Thread;

class Tag_thread extends Model {

    protected $table = 'tag_thread';

    protected $fillable = ['thread_id', 'tag_id'];

    public function tag() {
        return $this->belongsTo(Tag::class);
    }

    public function thread() {
        return $this->belongsTo(Thread::class);
    }
}
