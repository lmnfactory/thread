<?php

namespace Lmn\Thread\Database\Model;

use Illuminate\Database\Eloquent\Model;
use Lmn\Thread\Database\Model\Entryvote;
use Lmn\Account\Database\Model\User;

use Lmn\File\Database\Model\File;

class Entry extends Model {

    protected $table = 'entry';

    protected $fillable = ['user_id', 'thread_id', 'text', 'votevalue', 'firstentry'];

    public function thread() {
        return $this->belongsTo(Thread::class);
    }

    public function vote() {
        return $this->hasOne(Entryvote::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function files() {
        return $this->belongsToMany(File::class);
    }
}
