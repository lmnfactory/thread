<?php

namespace Lmn\Thread\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Entry_file extends Model {

    protected $table = 'entry_file';

    protected $fillable = ['entry_id', 'file_id'];
}
