<?php

namespace Lmn\Thread\Database\Model;

use Lmn\Thread\Database\Model\Entry;
use Lmn\Subject\Database\Model\Subject;
use Lmn\Tag\Database\Model\Tag;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model {

    protected $table = 'thread';

    protected $fillable = ['user_id', 'subject_id', 'title', 'entrycount'];

    public function subject() {
        return $this->belongsTo(Subject::class);
    }

    public function entries() {
        return $this->hasMany(Entry::class);
    }

    public function first_entry() {
        return $this->hasOne(Entry::class);
    }

    public function tags() {
        return $this->belongsToMany(Tag::class);
    }
}
